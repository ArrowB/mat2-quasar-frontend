import 'cypress-file-upload'
describe('Upload page tests', () => {
  beforeEach(() => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/*'
    }).as('api')
    cy.visit('/')
    cy.wait('@api').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'getting extensions successful')
    })
  })

  it('allows downloading multiple files', () => {
    cy.route({
      method: 'POST',
      url: '/api/*'
    }).as('fileUpload')

    let fileNameOne = 'test-image.png'
    cy.fixture(fileNameOne).then(fileContent => {
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        { fileContent: fileContent, fileName: fileNameOne, mimeType: 'image/png' },
        { subjectType: 'drag-n-drop' }
      )
    }).then(() => {
      cy.get('.uppy-Dashboard-files').contains(fileNameOne)
    })


    let fileNameTwo = 'test-image-two.png'
    cy.get('.uppy-DashboardContent-addMore').click()
    cy.fixture(fileNameTwo).then(fileContent => {
      cy.get('.uppy-DashboardTabs-list').upload(
        { fileContent: fileContent, fileName: fileNameTwo, mimeType: 'image/png' },
        { subjectType: 'drag-n-drop' }
      )
    }).then(() => {
      cy.get('.uppy-Dashboard-files').contains(fileNameTwo)
    })

    let fileNameThree = 'test-text-file.txt'
    cy.get('.uppy-DashboardContent-addMore').click()
    cy.fixture(fileNameThree).then(fileContent => {
      cy.get('.uppy-DashboardTabs-list').upload(
        { fileContent: fileContent, fileName: fileNameThree, mimeType: 'text/plain' },
        { subjectType: 'drag-n-drop' }
      )
    }).then(() => {
      cy.get('.uppy-Dashboard-files').contains(fileNameThree)
    }).then(() => {
      cy.get('.uppy-StatusBar-actions > button').click()
    })


    cy.wait('@fileUpload').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'file upload successful')
    })

    cy.get('[data-cy=removed-metadata-title]').should('have.text', 'Metadata removed')
    cy.get('[data-cy=removed-metadata-paragraph]').should(
      'have.text',
      'Successfully removed the metadata'
    )
    cy.get('[data-cy=main-back-button]').should('be.visible')
    cy.get('[data-cy=success-badge]').should('have.css', 'background-color')
      .and('eq', 'rgb(76, 175, 80)')

    cy.get('[data-cy=download-svg-icon]')
      .find('svg').should('have.length', 3)
    cy.get('[data-cy=card-download-link]')
      .eq(0).should('have.attr', 'href').and(
        'include',
        'test-image.cleaned.png'
      )
    cy.get('[data-cy=card-download-link]')
      .eq(1).should('have.attr', 'href').and(
        'include',
        'test-image-two.cleaned.png'
      )
    cy.get('[data-cy=card-download-link]')
      .eq(2).should('have.attr', 'href').and(
        'include',
        'test-text-file.cleaned.txt'
      )
  })

  it('should truncate the file name correctly', () => {
    cy.route({
      method: 'POST',
      url: '/api/*'
    }).as('fileUpload')

    let fileNameOne = 'a-very-long-name-for-a-very-small-text-file-to-truncate.txt'
    cy.fixture(fileNameOne).then(fileContent => {
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        { fileContent: fileContent, fileName: fileNameOne, mimeType: 'text/plain' },
        { subjectType: 'drag-n-drop' }
      )
    })
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@fileUpload').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'file upload successful')
    })

    cy.get('[data-cy=download-link]')
      .find('.q-btn__content')
      .contains('a-very-long-name...cate.cleaned.txt')
  })

  it('should display a download button for more than 3 files', () => {
    cy.fixture('test-text-file.txt', 'base64').then(txtFile => {
      const files = [
        { fileName: 'txt0.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt1.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt2.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt3.txt', fileContent: txtFile, mimeType: 'text/plain' }
      ]
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        files,
        { subjectType: 'drag-n-drop' }
      )
    })
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.get('[data-cy=failed-items-list]').should('not.be.visible')
    cy.get('[data-cy=zip-download-button] > .absolute-full > svg').should('be.visible')
    cy.get('[data-cy=zip-download-button]').contains('Download')
    cy.get('[data-cy=zip-download-button]').eq(0)
      .should('have.attr', 'href').and(
        'include',
        'cleaned.zip'
      )
  })
})

describe('Upload page negative tests', () => {
  it('lists files that could no be cleaned', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      response: ['.png', '.txt']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.route({
      method: 'POST',
      url: '/api/upload',
      status: 400,
      response: {
        error: 'some random error'
      }
    }).as('postFile')
    const fileName = 'a-very-long-name-for-a-very-small-text-file-to-truncate.txt'

    cy.fixture(fileName).then(fileContent => {
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        { fileContent: fileContent, fileName: fileName, mimeType: 'text/plain' },
        { subjectType: 'drag-n-drop' }
      )
    })
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@postFile')
    cy.get('[data-cy=failed-file-name]').eq(0).contains('a-very-long...runcate.txt')
    cy.get('[data-cy=failed-items-list]').should('be.visible')
  })

  it('redirects to error page if zip creation fails', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      response: ['.txt', '.jpg']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.route({
      method: 'POST',
      url: '/api/download/bulk',
      status: 400,
      response: {
        error: 'some random error'
      }
    }).as('zipCreation')

    cy.fixture('test-text-file.txt', 'base64').then(txtFile => {
      const files = [
        { fileName: 'txt0.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt1.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt2.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt3.txt', fileContent: txtFile, mimeType: 'text/plain' }
      ]
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        files,
        { subjectType: 'drag-n-drop' }
      )
    })
    cy.get('.uppy-StatusBar-actions > button').click()
    cy.wait('@zipCreation')
    cy.get('[data-cy=general-error-text]').contains('O Ooooh, Something went wrong')
  })
})
