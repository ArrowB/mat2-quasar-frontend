import 'cypress-file-upload'

describe('Landing', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('have the correct titles and descriptions', () => {
    cy.title().should('include', 'MAT2')
    cy.get('h1').contains('Remove Metadata')
    cy.get('p').contains(
      'The file you see is just the tip of the iceberg. Remove the hidden metadata with MAT2'
    )
    cy.get('.uppy-Dashboard-dropFilesTitle').contains(
      'Simply drag & drop, paste or use the file select button above.'
    )
    cy.get('[data-cy=footer-source-link]')
      .should('have.attr', 'href').and(
        'include',
        'https://0xacab.org/jfriedli/mat2-quasar-frontend'
      )
    cy.get('[data-cy=main-back-button]').should('not.be.visible')
  })
})

describe('Upload page tests', () => {
  beforeEach(() => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/*'
    }).as('api')
    cy.visit('/')
    cy.wait('@api').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'getting extensions successful')
    })
  })

  it('allows uploading a file', () => {
    const fileName = 'test-image.png'
    cy.route({
      method: 'POST',
      url: '/api/*'
    }).as('fileUpload')

    cy.fixture(fileName).then(fileContent => {
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        { fileContent, fileName, mimeType: 'image/png' },
        { subjectType: 'drag-n-drop' }
      )
    })

    cy.get('.uppy-DashboardItem-name').contains('test-image')
    cy.get('.uppy-StatusBar-actions > button').click()

    cy.wait('@fileUpload').then((xhr) => {
      assert.isNotNull(xhr.response.body.data, 'file upload successful')
    })

    cy.get('[data-cy=removed-metadata-title]').should('have.text', 'Metadata removed')
    cy.get('[data-cy=removed-metadata-paragraph]').should(
      'have.text',
      'Successfully removed the metadata'
    )
    cy.get('[data-cy=success-badge]').should('have.css', 'background-color')
      .and('eq', 'rgb(76, 175, 80)')
    cy.get('[data-cy=success-badge]').should('have.css', 'background-color')
      .and('eq', 'rgb(76, 175, 80)')
    cy.get('[data-cy=download-card]').find('svg')
      .should('be.visible')
  })

  it('rejects not supported file extensions', () => {
    const fileName = 'not-supported.json'

    cy.fixture(fileName).then(fileContent => {
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        { fileContent, fileName, mimeType: 'application/json' },
        { subjectType: 'drag-n-drop' }
      )
    })
    cy.wait(300)
    cy.get('.uppy-Informer').contains('You can only upload: .asc, .avi')
  })

  it('reject more than 10 files', () => {
    cy.fixture('test-text-file.txt', 'base64').then(txtFile => {
      const files = [
        { fileName: 'txt0.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt1.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt2.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt3.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt4.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt5.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt6.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt7.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt8.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt9.txt', fileContent: txtFile, mimeType: 'text/plain' },
        { fileName: 'txt10.txt', fileContent: txtFile, mimeType: 'text/plain' },
      ]
      cy.get('.uppy-Dashboard-dropFilesTitle').upload(
        files,
        { subjectType: 'drag-n-drop' }
      )
    })

    cy.wait(300)
    cy.get('.uppy-Informer').contains('You can only upload 10 files ')
  })
})

describe('Getting Extensions', () => {
  it('does fail and redirects to error page', () => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '/api/extension',
      status: 400,
      response: ['']
    }).as('supportedExtensions')
    cy.visit('/')
    cy.wait('@supportedExtensions')
    cy.get('[data-cy=general-error-text]').contains('O Ooooh, Something went wrong')
  })
})
