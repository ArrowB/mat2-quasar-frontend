export default {
  remove_metadata: 'Remove Metadata',
  the_file_you_see: 'The file you see is just the tip of the iceberg. Remove the hidden metadata with MAT2',
  info: 'Info',
  simply_drag: 'Simply drag & drop, paste or use the file select button above.',
  metadata_removed: 'Metadata removed',
  metadata_managed_to_remove: 'Successfully removed the metadata',
  removal_failed: 'Metadata removal failed',
  could_not_clean_files: 'Unable to clean the following files',
  loading_failed: 'Loading failed',
  four_o_four: 'There\'s no yarn here.',
  go_home: 'Back',
  mat_locally: 'MAT2 locally',
  mat_locally_info: 'Please note that while we do not keep a copy of your file, there is no way that you could be certain about this: Act accordingly. Due to this warning, please consider running MAT2 locally on your device.',
  mat_pip: 'MAT2 is available in pip',
  mat_debian: 'MAT2 on Debian',
  mat_debian_available: 'MAT2 is available on Debian',
  more_info: 'More info:',
  supported_formats: 'Supported file formats',
  bulk_download: 'Download',
  error_bulk_download_creation: 'Creating Zip file failed!',
  general_error: 'O Ooooh, Something went wrong',
  error_report: 'If you keep getting this error report it <a rel="noreferrer" href="https://0xacab.org/jfriedli/mat2-quasar-frontend/issues">here</a>',
  MAT2_metadata: 'What are metadata?',
  mat_what_is_metadata_1: 'Metadata consists of information that characterizes your file.\n' +
    'They answer background questions about who, how, when, and what.\n' +
    'This gives your file multi-faceted documentation.',
  mat_what_is_metadata_2: 'The metadata of your file leak a lot of information about you.\n' +
    'For example, cameras store file information about the time of a shot and which camera was used for it.\n' +
    'Documents, such as PDF or Words, automatically add author or company information to the document.\n' +
    'You don\'t want to publish all this information at all?',
  mat_what_is_metadata_3: 'This is precisely the job of MAT2: getting rid, as much as possible, of metadata.\n'
}
