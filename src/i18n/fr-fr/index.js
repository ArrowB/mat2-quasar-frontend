export default {
  remove_metadata: 'Supprimer les métadonnées',
  the_file_you_see: 'Le fichier visible n\'est que la pointe de l\'iceberg. Supprimer les métadonnées cachées avec MAT2.',
  info: 'Info',
  simply_drag: 'Glissez-déposez, insérez ou utilisez la sélection de fichiers.',
  metadata_removed: 'Métadonnées supprimées',
  metadata_managed_to_remove: 'Suppression réussie des métadonnées',
  removal_failed: 'Échec de la suppression des métadonnées',
  could_not_clean_files: 'Les fichiers suivants n\'ont pas pu être nettoyés:',
  loading_failed: 'Echec du chargement',
  four_o_four: 'Il n\'y a aucun mensonge ici.',
  go_home: 'Retour',
  mat_locally: 'MAT2 local',
  mat_locally_info: 'Même si nous n\'enregistrons pas une copie de votre fichier, vous ne pouvez jamais en être sûr: Agir en conséquence. Par conséquent, il est préférable d\'exécuter MAT2 localement sur votre périphérique.',
  mat_pip: 'MAT2 est disponible sous pip',
  mat_debian: 'MAT2 sur Debian',
  mat_debian_available: 'MAT2 est disponible sur Debian',
  more_info: 'Plus d\'informations:',
  supported_formats: 'Formats de fichiers pris en charge',
  bulk_download: 'Téléchargement',
  error_bulk_download_creation: 'La création du fichier zip a échoué!',
  general_error: 'O Ooooh, quelque chose a mal tourné',
  error_report: 'Si vous recevez toujours cette erreur, veuillez la signaler <a rel="noreferrer" href="https://0xacab.org/jfriedli/mat2-quasar-frontend/issues">ici</a>',
  MAT2_metadata: 'Qu\'est-ce que les métadonnées?',
  mat_what_is_metadata_1: 'Les métadonnées sont des informations qui caractérisent votre fichier.\n' +
    'Ils répondent aux questions de base: qui, comment, quand et quoi.\n' +
    'Votre dossier reçoit ainsi une documentation multi-facettes.',
  mat_what_is_metadata_2: 'Les métadonnées de votre fichier contiennent beaucoup d\'informations sur vous.\n' +
    'Par exemple, les caméras stockent des informations de fichier sur l\'heure d\'une prise de vue et sur la caméra qui a été utilisée pour cette prise de vue.\n' +
    'Les documents Office tels que PDF ou Words ajoutent automatiquement des informations d\'auteu.r.ice ou d\'entreprise au document.\n' +
    'Tu ne veux pas publier toutes ces informations?',
  mat_what_is_metadata_3: 'Ici MAT2 peut t\'aider: Il supprime autant de métadonnées que possible.\n'
}
