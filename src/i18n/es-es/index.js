export default {
  remove_metadata: 'Eliminar metadatos',
  the_file_you_see: 'El archivo visible es sólo la punta del iceberg. Elimine los metadatos ocultos con MAT2',
  info: 'información',
  simply_drag: 'Arrastrar y soltar, Insertar o utilizar la selección de archivos.',
  metadata_removed: 'Metadatos eliminados',
  metadata_managed_to_remove: 'Metadatos eliminados con éxito',
  removal_failed: 'Error en la eliminación de metadatos',
  could_not_clean_files: 'Los siguientes archivos no se pudieron limpiar:',
  loading_failed: 'Fallo en la carga',
  four_o_four: 'Aquí no hay hilo.',
  go_home: 'Espalda',
  mat_locally: 'MAT2 local',
  mat_locally_info: 'Incluso si no guardamos una copia de su archivo, nunca podrá estar seguro: Actúa en consecuencia. Por lo tanto, es mejor ejecutar MAT2 localmente en su dispositivo.',
  mat_pip: 'MAT2 está disponible bajo pip',
  mat_debian: 'MAT2 en Debian',
  mat_debian_available: 'MAT2 está disponible en Debian',
  more_info: 'Más información:',
  supported_formats: 'Formatos de archivo compatibles',
  bulk_download: 'Descargar',
  error_bulk_download_creation: 'Error al crear el archivo zip!',
  general_error: 'O Ooooh, algo salió mal',
  error_report: 'Si todavía recibe este error, por favor repórtelo <a rel="noreferrer" href="https://0xacab.org/jfriedli/mat2-quasar-frontend/issues">aquí</a>',
  MAT2_metadata: '¿Qué son los metadatos?',
  mat_what_is_metadata_1: 'Los metadatos consisten en información que caracteriza su archivo.\n' +
    'Responden a las preguntas de fondo: quién, cómo, cuándo y qué.\n' +
    'De este modo, su expediente recibe una documentación polifacética.',
  mat_what_is_metadata_2: 'Los metadatos de su archivo dan mucho precio sobre usted.\n' +
    'Por ejemplo, las cámaras almacenan información de archivo sobre cuándo se tomó una toma y qué cámara se utilizó.\n' +
    'Documentos como PDF o Docx añaden automáticamente información sobre el autor o la empresa al documento.\n' +
    '¿No quieres publicar toda esta información?',
  mat_what_is_metadata_3: 'Aquí es donde MAT2 puede ayudar: Elimina tantos metadatos como sea posible.\n'
}
