export default {
  remove_metadata: 'Rimuovi metadati',
  the_file_you_see: 'La lima visibile è solo la punta dell\'iceberg. Rimuovere i metadati nascosti con MAT2.',
  info: 'Info',
  simply_drag: 'Trascinare e rilasciare, inserire o utilizzare la selezione del file.',
  metadata_removed: 'Metadati rimossi',
  metadata_managed_to_remove: 'Metadati rimossi con successo',
  removal_failed: 'Rimozione dei metadati fallita',
  could_not_clean_files: 'Non è stato possibile pulire i seguenti file:',
  loading_failed: 'Caricamento fallito',
  four_o_four: 'Non c\'è filato qui.',
  go_home: 'Indietro',
  mat_locally: 'MAT2 locale',
  mat_locally_info: 'Anche se non salviamo una copia del tuo file, non potrai mai esserne sicuro: Agire di conseguenza. Quindi e\' meglio eseguire MAT2 localmente sul tuo dispositivo.',
  mat_pip: 'MAT2 è disponibile sotto pip',
  mat_debian: 'MAT2 su Debian',
  mat_debian_available: 'MAT2 è disponibile su Debian',
  more_info: 'Maggiori informazioni:',
  supported_formats: 'Formati di file supportati',
  bulk_download: 'Scaricamento',
  error_bulk_download_creation: 'La creazione del file zip è fallita!',
  general_error: 'O Ooooh, qualcosa è andato storto.',
  error_report: 'Se si riceve ancora questo errore, si prega di segnalarlo <a rel="noreferrer" href="https://0xacab.org/jfriedli/mat2-quasar-frontend/issues">qui</a>',
  MAT2_metadata: 'Che cosa sono i metadati?',
  mat_what_is_metadata_1: 'I metadati sono costituiti da informazioni che caratterizzano il vostro file.\n' +
    'Rispondono alle domande di fondo: chi, come, come, quando e cosa.\n' +
    'Il file riceve così una documentazione sfaccettata.',
  mat_what_is_metadata_2: 'I metadati del vostro file danno un sacco di prezzo su di voi.\n' +
    'Ad esempio, le telecamere memorizzano informazioni sui file relativi a quando è stato scattato uno scatto e a quale telecamera è stata utilizzata.\n' +
    'Documenti come PDF o Docx aggiungono automaticamente al documento informazioni sull\'autore o sull\'azienda.\n' +
    'Non vuoi pubblicare tutte queste informazioni?',
  mat_what_is_metadata_3: 'Qui è dove MAT2 può aiutare: Elimina il maggior numero possibile di metadati.\n'
}
