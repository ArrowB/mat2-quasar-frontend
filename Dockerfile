# develop stage
FROM node:12 as develop-stage
WORKDIR /app
COPY package*.json ./
RUN yarn global add @quasar/cli
COPY . .
# build stage
FROM develop-stage as build-stage
WORKDIR /app
ARG MAT2_API_URL_PROD
ENV MAT2_API_URL_PROD=$MAT2_API_URL_PROD
RUN yarn && \
    quasar build -m pwa
# production stage
# https://jonathanmh.com/deploying-a-vue-js-single-page-app-including-router-with-docker/
# https://www.georg-ledermann.de/blog/2018/04/27/dockerize-and-configure-javascript-single-page-application/
FROM nginx:1.17.6-alpine as production-stage
RUN mkdir -p /tmp/nginx/vue-single-page-app && \
    mkdir -p /var/log/nginx && \
    mkdir -p /var/www/html
COPY --from=build-stage /app/dist/pwa /var/www/html
COPY nginx_config/default.conf /etc/nginx/conf.d/default.conf
COPY nginx_config/nginx.conf /etc/nginx/nginx.conf
COPY ./entrypoint.sh /tmp/
EXPOSE 80

CMD ["/tmp/entrypoint.sh"]
