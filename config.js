// config for renovate! not quasar
module.exports = {
  platform: 'gitlab',
  endpoint: 'https://0xacab.org/api/v4/',
  token: process.env.RENOVATE_TOKEN,

  repositories: [
    'jfriedli/mat2-quasar-frontend'
  ],

  logLevel: 'info',

  requireConfig: true,
  onboarding: true,
  onboardingConfig: {
    extends: ['config:base'],
    prConcurrentLimit: 5
  },

  enabledManagers: [
    'yarn'
  ]
}
